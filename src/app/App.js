import React, {Fragment} from 'react'

import ExchangeContainer from '../exchange/ExchangeContainer'

class App extends React.Component {
    render() {
        return (
            <Fragment>
            <header>
                <div className="navbar navbar-dark bg-dark box-shadow">
                    <div className="container d-flex justify-content-between">
                    <a href="/" className="navbar-brand d-flex align-items-center">
                        <strong>moneyxchange.io</strong>
                    </a>
                    </div>
                </div>
            </header>

            <main role="main">
                <section className="jumbotron text-center">
                    <div className="container">
                        <ExchangeContainer />
                    </div>
                </section>
            </main>
            </Fragment>
        )
    }
}

export default App