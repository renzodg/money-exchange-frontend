import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'

const defaultState = {
    exchange : {
        targetAmount : null,
        baseAmount : null,
        error: false
    }
  }
  
const enhancers = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)

const store = createStore(reducer, {}, enhancers)

export default store