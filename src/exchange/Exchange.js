import React from 'react'
import CurrencyInput from 'react-currency-input'

class Exchange extends React.Component {

    handleCalculate() {
        const baseAmountValue = this.refs.baseAmount.getMaskedValue().replace(/,/g , '')
                                                                    .replace('$', '')
        this.props.handleCalculate(baseAmountValue)
    }

    renderError() {
        if (this.props.error) {
            return (
                <div className="alert alert-danger" role="alert">
                    Parece que hay problemas con el servidor. Intenta más tarde
                </div>
            )
        }
    }

    render() {
        return (
                
            <div>  
                {this.renderError()}
                <div className="row">
                    <div className="col">
                        <CurrencyInput  ref="baseAmount" placeholder="USD" value={this.props.baseAmount}
                            className="form-control currency-input" precision="4" prefix="$ " />
                    </div>
                    <div className="col">
                        <CurrencyInput  type="text" value={this.props.targetAmount} disabled placeholder="EUR"
                            className="form-control currency-input" precision="4" prefix="€ "/>
                    </div>
                </div>
                <button className="btn btn-primary btn-lg btn-block" style={{marginTop: 30}}
                        onClick={this.handleCalculate.bind(this)}>Calculate</button>
            </div>
        )
    }
}

export default Exchange