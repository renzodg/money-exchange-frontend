import React from 'react'
import { connect } from 'react-redux'
import { getRatesAndCalculate, calculateExchange } from './ExchangeActions'
import Exchange from './Exchange'
import { bindActionCreators } from 'redux'

class ExchangeContainer extends React.Component {
        checkToFetchRates(lastRatesSync) {
            if (!lastRatesSync) {
                return true
            }

            const diffInMilliseconds = new Date() - this.props.lastRatesSync
            const diffInMinutes = diffInMilliseconds / 1000 / 60
            return diffInMinutes >= 10
        }

        handleCalculate(baseAmount) {
            const fetch = this.checkToFetchRates(this.props.lastRatesSync)

            if (fetch) {
                this.props.getRatesAndCalculate(baseAmount)
            } else {
                this.props.calculateExchange(baseAmount)
            }
        }

        

        render() {
            return (
               <Exchange handleCalculate={this.handleCalculate.bind(this)} error={this.props.error}
                targetAmount={this.props.targetAmount} baseAmount={this.props.baseAmount} />
            )
        }
}

const mapStateToProps = (state) => {
    return {
        ...state.exchange
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getRatesAndCalculate, calculateExchange }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeContainer)