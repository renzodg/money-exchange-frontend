import * as exchangeConstats from './ExchangeConstants'

const defaultState =  {
    targetAmount : null,
    baseAmount : null,
    error: false
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case exchangeConstats.FETCH_RATES_PENDING: 
            return {
                ...state,
                fetching : true,
                baseAmount: action.payload.baseAmount
            }
        case exchangeConstats.FETCH_RATES_FULFILLED: 
            return {
                ...state,
                exchangeRates : action.payload.exchangeRates,
                lastRatesSync : new Date(),
                fetching : false,
                error : false
            }
        case exchangeConstats.FETCH_RATES_REJECTED:
            return {
                ...state,
                error : true
            }
        case exchangeConstats.CALCULATE_EXCHANGE:
            return {
                ...state,
                baseAmount: action.payload.baseAmount,
                targetAmount: action.payload.baseAmount * state.exchangeRates.rates['EUR']
            }
        default:
            return state
    }
}