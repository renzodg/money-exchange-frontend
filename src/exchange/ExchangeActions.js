import axios from 'axios'

import * as exchangeConstats from './ExchangeConstants'

export function getRatesAndCalculate(baseAmount) {
    return dispatch => {
        dispatch(fetchRatesPending(baseAmount))
        return axios.get('http://localhost:8080/latest?base=USD&symbol=EUR')
        .then(response => {
            dispatch(fetchRatesFulFilled(response.data))
            dispatch(calculateExchange(baseAmount))})
        .catch(error => {
            dispatch(handleError(error))
        })
    }
}

export function handleError(error) {
    return {
        type: exchangeConstats.FETCH_RATES_REJECTED
    }
}

export function fetchRatesPending(baseAmount) {
    return {
        type: exchangeConstats.FETCH_RATES_PENDING,
        payload: {
            baseAmount
        }
    }
}

export function fetchRatesFulFilled(exchangeRates) {
    return {
        type: exchangeConstats.FETCH_RATES_FULFILLED,
        payload: {
            exchangeRates
        }
    }
}

export function calculateExchange(baseAmount) {
    return {
        type: exchangeConstats.CALCULATE_EXCHANGE,
        payload: {
            baseAmount
        }
    }
}