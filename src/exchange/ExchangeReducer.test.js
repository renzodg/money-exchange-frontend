import reducer from './ExchangeReducer'
import * as types from './ExchangeConstants'

describe('exchange reducer', () => {

    const state =  {
        targetAmount : null,
        baseAmount : null,
        exchangeRates: {
            rates: {
                EUR: 0.81241
            }
        }
    }
 
    it('should handle CALCULATE_EXCHANGE', () => {
        expect(reducer(state, {
            type: types.CALCULATE_EXCHANGE,
            payload: {
                baseAmount : 1
            }
        }))
        .toEqual({
            ...state,
            baseAmount: 1,
            targetAmount: 0.81241
        })

        
    })
})