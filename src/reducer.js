import { combineReducers } from 'redux'
import exchangeReducer from './exchange/ExchangeReducer'
 
export default combineReducers({
  exchange: exchangeReducer
})